﻿using AutoMapper;
using Meimo.Services.EmailNotifier.Api.Models.Api;
using Meimo.Services.EmailNotifier.Api.Models.Service;
using Meimo.Services.EmailNotifier.Core.Domain.Notification;
using Meimo.Services.EmailNotifier.Infrastructure.Contracts;

namespace Meimo.Services.EmailNotifier.Api.Infrastructure.AutoMapper
{
    public class EmailMapperProfile: Profile
    {
        public EmailMapperProfile()
        {
            CreateMap<SendEmailRequest, SendingEmail>();
            CreateMap<SendingEmail, RmqSendEmailMessage>();
            CreateMap<EmailTemplate, EmailTemplateShortResponse>();
            CreateMap<CreateEmailTemplateRequest, EmailTemplate>()
                .ForMember(p=>p.TemplateFields,
                    opt=> opt.MapFrom(s=>s.TemplateFields));
            CreateMap<EmailTemplateFieldRequest, EmailTemplateField>();
            CreateMap<UpdateEmailTemplateRequest, EmailTemplate>();
            CreateMap<EmailTemplate, EmailTemplateResponse>().ForMember(p=>p.TemplateFields,
                opt=> opt.MapFrom(s=>s.TemplateFields));
            CreateMap<EmailTemplateField, EmailTemplateFieldResponse>();
            CreateMap<SendEmailTemplateRequest, SendingEmailTemplate>();
            CreateMap<SendingEmailTemplateParameterRequest, SendingEmailTemplateParameter>();
            CreateMap<SendingEmailTemplate, SendingEmail>().ForMember(p=>p.Body, opt=> opt.Ignore());
        }
    }
}