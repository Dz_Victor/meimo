﻿namespace Meimo.Services.EmailNotifier.Api.Models.Service
{
    public class SendingEmailTemplateParameter
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}