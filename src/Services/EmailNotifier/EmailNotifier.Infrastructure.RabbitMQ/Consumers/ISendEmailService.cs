﻿using System.Threading.Tasks;
using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;
using Meimo.Services.EmailNotifier.Infrastructure.Contracts;

namespace Meimo.Services.EmailNotifier.Infrastructure.Consumers
{
    public interface ISendEmailService
    {
        void SendEmail(TracedObject<RmqSendEmailMessage> sendEmailMessageRmq);
    }
}