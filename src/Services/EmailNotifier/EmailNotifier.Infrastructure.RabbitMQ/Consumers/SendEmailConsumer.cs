﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;
using Meimo.Services.EmailNotifier.Infrastructure.Contracts;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Meimo.Services.EmailNotifier.Infrastructure.Consumers
{
    public class SendEmailConsumer : ISendEmailConsumer
    {
        private readonly string _hostname;
        private readonly string _password;
        private readonly string _queueName;
        private readonly string _username;
        private IConnection _connection;
        private readonly ILogger<SendEmailConsumer> _logger;
        private readonly ISendEmailService _sendEmailService;
        private IModel _channel;
        private string _tag;
        private ITraceLogger _traceLogger;

        public SendEmailConsumer(ILogger<SendEmailConsumer> logger, ISendEmailService sendEmailService,
            IOptions<RabbitMqConfiguration> rabbitMqOptions, ITraceLogger traceLogger)
        {
            _logger = logger;
            _hostname = rabbitMqOptions.Value.Hostname;
            _queueName = rabbitMqOptions.Value.QueueName;
            _username = rabbitMqOptions.Value.Username;
            _password = rabbitMqOptions.Value.Password;
            _sendEmailService = sendEmailService;
            _traceLogger = traceLogger;
            InitializeRabbitMqListener();
        }

        private void InitializeRabbitMqListener()
        {
            var factory = new ConnectionFactory
            {
                HostName = _hostname,
                UserName = _username,
                Password = _password
            };

            _connection = factory.CreateConnection();
            _connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: _queueName, durable: false, exclusive: false, autoDelete: false,
                arguments: null);
        }

        private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e)
        {

        }

        public Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                var sendEmailMessage = JsonConvert.DeserializeObject<TracedObject<RmqSendEmailMessage>>(content);
                _traceLogger.TraceLog($"{nameof(SendEmailConsumer)}.{nameof(EventingBasicConsumer.Received)}", sendEmailMessage);
                HandleMessage(sendEmailMessage);
                
                _channel.BasicAck(ea.DeliveryTag, false);
            };
            consumer.Shutdown += OnConsumerShutdown;
            consumer.Registered += OnConsumerRegistered;
            consumer.Unregistered += OnConsumerUnregistered;
            consumer.ConsumerCancelled += OnConsumerCancelled;

            _tag = _channel.BasicConsume(_queueName, false, consumer);

            return Task.CompletedTask;
        }

        public Task StopExecuteAsync(CancellationToken cancellationToken)
        {
            _channel.BasicCancel(_tag);
            
            return Task.CompletedTask;
        }

        private void OnConsumerCancelled(object sender, ConsumerEventArgs e)
        {
            _logger.Log(LogLevel.Debug, nameof(OnConsumerCancelled));
        }

        private void OnConsumerUnregistered(object sender, ConsumerEventArgs e)
        {
            _logger.Log(LogLevel.Debug, nameof(OnConsumerUnregistered));
        }

        private void OnConsumerRegistered(object sender, ConsumerEventArgs e)
        {
            _logger.Log(LogLevel.Debug, nameof(OnConsumerRegistered));
        }

        private void OnConsumerShutdown(object sender, ShutdownEventArgs e)
        {
            _logger.Log(LogLevel.Debug, nameof(OnConsumerShutdown));
        }

        private void HandleMessage(TracedObject<RmqSendEmailMessage> sendEmailMessageRmq)
        {
            try
            {
                _logger.Log(LogLevel.Debug,$"Try to send message to: '{sendEmailMessageRmq.Data.To}'");
                _sendEmailService.SendEmail(sendEmailMessageRmq);
                _traceLogger.TraceLog($"Successed sent email", sendEmailMessageRmq);
            }
            catch (Exception e)
            {
                _logger.LogError($"Failed to send message to: '{sendEmailMessageRmq.Data.To}' with error: '{e.Message}'");
                _traceLogger.TraceLog($"Failed sent email", sendEmailMessageRmq);
            }
            
        }

        public void Dispose()
        {
            _channel.Close();
            _connection.Close();
        }
    }
}