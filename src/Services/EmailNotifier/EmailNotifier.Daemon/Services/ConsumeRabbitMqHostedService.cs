﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Meimo.Services.EmailNotifier.Infrastructure.Consumers;
using Microsoft.Extensions.Hosting;

namespace Meimo.Services.EmailNotifier.Daemon.Services
{
    public class ConsumeRabbitMqHostedService: IHostedService, IDisposable
    {
        private ISendEmailConsumer _sendEmailConsumer;
        public ConsumeRabbitMqHostedService(ISendEmailConsumer sendEmailConsumer)
        {
            _sendEmailConsumer = sendEmailConsumer;
        }
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _sendEmailConsumer.ExecuteAsync(cancellationToken);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _sendEmailConsumer.StopExecuteAsync(cancellationToken);
        }

        public void Dispose()
        {
            _sendEmailConsumer.Dispose();
        }
    }
}