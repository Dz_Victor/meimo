﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Meimo.Services.EmailNotifier.Core.Domain;

namespace Meimo.Services.EmailNotifier.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<T> AddAsync(T newEntity);

        Task<T> UpdateAsync(T updatedEntity);

        void DeleteAsync(Guid id);

    }
}