﻿using System.ComponentModel.DataAnnotations;

namespace Meimo.Services.EmailNotifier.Core.Domain.Notification
{
    public class EmailTemplateField:BaseEntity
    {
        [Required]
        [MaxLength(50)]
        public string Key { get; set; }
        
        public EmailTemplate Template { get; set; }
    }
}