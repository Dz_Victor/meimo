﻿using System;
using System.Text.Json;
using Microsoft.Extensions.Logging;

namespace Meimo.Services.EmailNotifier.Core.Infrastructure.Logging
{
    public class TraceLogger:ITraceLogger
    {
        private ILogger<TraceLogger> _logger;
        
        public TraceLogger(ILogger<TraceLogger> logger)
        {
            _logger = logger;
        }

        public void TraceLog<T>(string operationName, TracedObject<T> data)
        {
            TraceLog(data.TraceId, operationName, JsonSerializer.Serialize(data.Data));
        }

        public void TraceLog(Guid traceId, string operationName, string message)
        {
            _logger.LogInformation("{traceId} {operationName} {data}",traceId, operationName, message);
        }
    }
}