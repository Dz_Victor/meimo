
using Meimo.Person.Account.Infrastructure.Data;
using Meimo.Person.Account.WebApi.Errors;
using Meimo.Person.Account.WebApi.Extensions;
using Meimo.Person.Account.WebApi.Middleware;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Meimo.Person.Account.WebApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddRouting(options => 
            {
                options.LowercaseUrls = true;
            });

            services.AddDbContext<StoreContext>(x =>
                    x.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddMvc(o =>
            {
                o.Filters.Add(new ProducesResponseTypeAttribute(typeof(ApiException), 
                    StatusCodes.Status500InternalServerError));
            });

            services.AddApplicationServices(Configuration);

            services.AddSwaggerDocumentation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<ExceptionMiddleware>();
            app.UseStatusCodePagesWithReExecute("/errors/{0}");

            app.UseSwaggerDocumentation();

            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
