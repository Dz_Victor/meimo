﻿using System.Collections.Generic;

namespace Meimo.Person.Account.WebApi.Errors
{
    public class ModelErrorKeyValues
    {
        public string Key { get; set; }

        public IEnumerable<string> Values { get; set; }

        public ModelErrorKeyValues(string key, IEnumerable<string> values)
        {
            Key = key;
            Values = values;
        }
    }
}
