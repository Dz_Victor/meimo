﻿using System.ComponentModel.DataAnnotations;

namespace Meimo.Person.Account.WebApi.Models
{
    public class ChangePhoneRequest
    {
        [Required]
        [MaxLength(30), MinLength(5)]
        [Phone]
        public string Phone { get; set; }

        [Required]
        [MaxLength(30), MinLength(5)]
        public string Password { get; set; }
    }
}
