﻿
using Meimo.Person.Account.Core.Entities;

using Meimo.Person.Account.Core.Helpers;

namespace Meimo.Person.Account.WebApi.Models
{
    public class GenderResponse
    {
        public short Id { get; set; }

        public string Name { get; set; }

        public string InvariantName { get; set; }

        public GenderResponse(Gender gender)
        {
            Id = (short)gender;
            Name = gender.GetEnumMember();
            InvariantName = gender.ToString();
        }
    }
}
