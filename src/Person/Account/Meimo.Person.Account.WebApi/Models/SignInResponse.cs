﻿using System;

namespace Meimo.Person.Account.WebApi.Models
{
    public class SignInResponse
    {
        public Guid PersonId { get; set; }

        public SignInResponse(Guid personId)
        {
            PersonId = personId;
        }
    }
}
