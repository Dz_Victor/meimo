﻿using System.Threading.Tasks;

using Meimo.Person.Account.Core.Interfaces;
using Meimo.Person.Account.EmailNotifierClient;
using Meimo.Person.Account.WebApi.Errors;
using Meimo.Person.Account.WebApi.Helpers;
using Meimo.Person.Account.WebApi.Models;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Meimo.Person.Account.WebApi.Controllers
{
    public class ActivationTokensController : BaseApiController
    {
        private readonly ILogger<ActivationTokensController> _logger;
        private readonly IEmailSender _emailSender;
        private readonly IAccountService _accountService;
        private readonly IActivationTokenService _activationTokenService;
        private readonly EmailActivationOptions _emailActivationOptions;

        public ActivationTokensController(
            ILogger<ActivationTokensController> logger,
            IEmailSender emailSender,
            IAccountService accountService,
            IActivationTokenService activationTokenService, 
            EmailActivationOptions emailActivationOptions)
        {
            _logger = logger;
            _emailSender = emailSender;
            _accountService = accountService;
            _activationTokenService = activationTokenService;
            _emailActivationOptions = emailActivationOptions;
        }

        /// <summary>
        /// Сгенерировать и отправить новый активационный токен
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ApiResponseExtended), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status409Conflict)]
        public async Task<ActionResult> MakeAndSendNewTokenAsync(
            [FromBody] MakeNewTokenRequest request)
        {
            if (request == null)
            {
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest,
                    $"{nameof(MakeNewTokenRequest)} object is null"));
            }

            var personAccount = await _accountService
                .GetAccountByLoginAndPasswordAsync(request.Login, request.Password);

            if (personAccount == null)
            {
                return NotFound(new ApiResponse(StatusCodes.Status400BadRequest,
                    "Неверное имя пользователя или пароль"));
            }

            if (personAccount.Activated)
            {
                return NotFound(new ApiResponse(StatusCodes.Status400BadRequest,
                    "Профиль уже был активирован ранее"));
            }

            if (!await _activationTokenService.MakeNewTokenAsync(personAccount.Id))
            {
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict,
                    "При создании активационного токена произошла непредвиденная ошибка," +
                    " повторите попытку позже"));
            }

            if (!await SendActivationEmailHelper.SendActivationRequestAsync(
                    _emailSender,
                    _logger,
                    _emailActivationOptions,
                    personAccount.Email,
                    await _activationTokenService.GetTokenByIdAsync(personAccount.Id)))
            {
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict,
                    "При отправке письма для активации аккаунта произошла непредвиденная ошибка," +
                    " повторите попытку позже"));
            }

            return NoContent();
        }
    }
}
