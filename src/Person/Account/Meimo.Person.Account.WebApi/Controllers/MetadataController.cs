﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Meimo.Person.Account.Core.Entities;
using Meimo.Person.Account.Core.Interfaces;
using Meimo.Person.Account.Infrastructure.Data;
using Meimo.Person.Account.WebApi.Errors;
using Meimo.Person.Account.WebApi.Models;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Meimo.Person.Account.WebApi.Controllers
{
    public class MetadataController : BaseApiController
    {
        private readonly ICountryService _countryService;

        public MetadataController(StoreContext context, ICountryService countryService)
        {
            _countryService = countryService;
        }

        /// <summary>
        /// Получить список возможных значений пола пользователей
        /// </summary>
        [HttpGet("genders")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IReadOnlyList<GenderResponse>>> GetGendersAsync()
        {
            var task = Task.FromResult(
                Enum.GetValues(typeof(Gender))
                    .Cast<Gender>()
                    .Select(x => new GenderResponse(x))
                    .OrderBy(x => x.Id)
                    .ToList());

            return Ok(await task);
        }

        /// <summary>
        /// Получить пол по id
        /// </summary>
        [HttpGet("genders/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponseExtended), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GenderResponse>> GetGenderAsync(short id)
        {
            if (!Enum.IsDefined(typeof(Gender), id))
            {
                return NotFound(new ApiResponse(StatusCodes.Status404NotFound));
            }

            return Ok(await Task.FromResult(new GenderResponse((Gender)id)));
        }

        /// <summary>
        /// Получить список стран
        /// </summary>
        [HttpGet("countries")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IReadOnlyList<CountryResponse>>> GetCountriesAsync()
        {
            var response = (await _countryService.GetCountriesAsync())
                .Select(c => new CountryResponse(c))
                .ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить страну по id
        /// </summary>
        [HttpGet("countries/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponseExtended), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CountryResponse>> GetCountryAsync(int id)
        {
            var country = await _countryService.GetCountryByIdAsync(id);

            if (country == null)
            {
                return NotFound(new ApiResponse(StatusCodes.Status404NotFound));
            }

            return Ok(new CountryResponse(country));
        }
    }
}
