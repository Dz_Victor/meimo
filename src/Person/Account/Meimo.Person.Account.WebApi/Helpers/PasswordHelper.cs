﻿using System.ComponentModel.DataAnnotations;

namespace Meimo.Person.Account.WebApi.Helpers
{
    public class PasswordHelper
    {
        public static ValidationResult ValidatePasswordDifference(
            string password,
            string oldPassword,
            string passworMemberName)
        {
            if (password == oldPassword)
            {
                return new ValidationResult(
                    "Старый и новый пароли должны различаться",
                    new[] { passworMemberName });
            }

            return ValidationResult.Success;
        }

        public static ValidationResult ValidatePasswordConfirmation(
            string password,
            string passwordConfirmation,
            string passworMemberName,
            string passwordConfirmationMemberName)
        {
            if (password != passwordConfirmation)
            {
                return new ValidationResult(
                    "Введенные пароли не совпадают",
                    new[] { passworMemberName, passwordConfirmationMemberName });
            }

            return ValidationResult.Success;
        }
    }
}
