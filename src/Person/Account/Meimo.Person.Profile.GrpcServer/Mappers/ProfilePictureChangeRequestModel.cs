﻿using System;

using Meimo.Person.Account.Core.Dtos;

namespace Meimo.Person.Profile.GrpcServer
{
    public sealed partial class ProfilePictureChangeRequestModel
    {
        public static PictureInfoUpdateDto ToRepositoryModel(ProfilePictureChangeRequestModel source)
        {
            if (source is null) return null;

            return new PictureInfoUpdateDto
            {
                Id = Guid.Parse(source.Id),
                Avatar = source.Avatar,
                Picture = source.Picture
            };
        }
    }
}
