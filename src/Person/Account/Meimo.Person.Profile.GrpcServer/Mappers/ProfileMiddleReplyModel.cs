﻿using Meimo.Person.Account.Core.Dtos;

namespace Meimo.Person.Profile.GrpcServer
{
    public sealed partial class ProfileMiddleReplyModel
    {
        public static ProfileMiddleReplyModel FromRepositoryModel(ProfileMiddleInfoDto source)
        {
            if (source is null) return null;

            return new ProfileMiddleReplyModel
            {
                Id = source.Id.ToString(),
                FirstName = source.FirstName,
                LastName = source.LastName,
                Avatar = source.Avatar,
                City = source.City,
                Country = source.Country,
                Locked = source.Locked,
                Deleted = source.Deleted
            };
        }
    }
}
