﻿using System;

using Grpc.Core;

using Meimo.Person.Account.Core.Entities;

namespace Meimo.Person.Profile.GrpcServer
{
    public sealed partial class ProfileChangeRequestModel
    {
        public static void Validate(ProfileChangeRequestModel model)
        {
            if (!Guid.TryParse(model.Id, out _))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Profile Id must be a GUID"));
            }

            if (string.IsNullOrWhiteSpace(model.FirstName))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    "First Name cannot be null or white space"));
            }

            if (model.FirstName != null && model.FirstName.Length > 100)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    "First Name cannot be greater than 100 characters"));
            }

            if (model.LastName != null && model.LastName.Length > 200)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    "Last Name cannot be greater than 200 characters"));
            }

            if (model.Gender > short.MaxValue || !Enum.IsDefined(typeof(Gender), (short)model.Gender))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    "Gender must be a value from the Gender enumeration"));
            }

            if (model.City.Length > 100)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    "City name cannot be greater than 100 characters"));
            }
        }
    }
}
