﻿namespace Meimo.Person.Account.Core.Entities
{
    public enum SysRole
    {
        None = 0,
        Sys = 1,
        Sysop = 2
    }
}
