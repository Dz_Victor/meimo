﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Meimo.Person.Account.Core.Entities
{
    public class PersonAccount
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(30), MinLength(5)]
        [Phone]
        public string Phone { get; set; }

        [MaxLength(100), MinLength(5)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MaxLength(30), MinLength(5)]
        public string Password { get; set; }

        [Required]
        public DateTimeOffset PasswordDate { get; set; }

        [Required]
        [MaxLength(100), MinLength(1)]
        public string FirstName { get; set; }

        [MaxLength(200)]
        public string LastName { get; set; }

        [Required]
        public Gender Gender { get; set; }

        [Required]
        public bool ShowGender { get; set; }

        public DateTimeOffset? Birthday { get; set; }

        [Required]
        public bool ShowBirthday { get; set; }

        [Required]
        public int CountryId { get; set; }

        public Country Country { get; set; }

        [Required]
        public SysLanguage SysLanguage { get; set; }

        [MaxLength(100)]
        public string City { get; set; }

        public string Avatar { get; set; }

        public string Picture { get; set; }

        public SysRole? SysRole { get; set; }

        [Required]
        public bool Activated { get; set; }

        public bool Locked { get; set; }

        public bool Deleted { get; set; }
    }
}
