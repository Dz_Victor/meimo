﻿using System;

namespace Meimo.Person.Account.Core.Dtos
{
    public class LoginDto
    {
        public string Phone { get; set; }

        public string Email { get; set; }

        public LoginDto(string phone, string email)
        {
            Phone = phone;
            Email = email;
        }
    }
}
