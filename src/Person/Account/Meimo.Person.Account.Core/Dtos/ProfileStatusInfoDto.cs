﻿namespace Meimo.Person.Account.Core.Dtos
{
    public class ProfileStatusInfoDto
    {
        public bool AccessIsAllowed { get; }
        public bool Activated { get; }
        public bool Locked { get; }
        public bool Deleted { get; }

        private ProfileStatusInfoDto()
        {

        }

        public ProfileStatusInfoDto(bool activated, bool locked, bool deleted)
        {
            Activated = activated;
            Locked = locked;
            Deleted = deleted;
            AccessIsAllowed = Activated && !Locked && !Deleted;
        }
    }
}
