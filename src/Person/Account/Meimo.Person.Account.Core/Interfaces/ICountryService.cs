﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Meimo.Person.Account.Core.Entities;

namespace Meimo.Person.Account.Core.Interfaces
{
    public interface ICountryService
    {
        Task<List<Country>> GetCountriesAsync();

        Task<Country> GetCountryByIdAsync(int id);

        Task<bool> HasCountryAsync(int id);
    }
}
