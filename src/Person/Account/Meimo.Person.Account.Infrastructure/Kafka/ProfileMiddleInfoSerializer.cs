﻿using System;
using System.Text.Json;

using Confluent.Kafka;

using Meimo.Person.Account.Core.Dtos;

namespace Meimo.Person.Account.Infrastructure.Kafka
{
    class ProfileMiddleInfoSerializer : ISerializer<ProfileMiddleInfoDto>
    {
        private readonly JsonSerializerOptions _jsonSerializerOptions;

        public ProfileMiddleInfoSerializer()
        {
            _jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                IgnoreNullValues = true,
                WriteIndented = true,
            };
        }

        public byte[] Serialize(ProfileMiddleInfoDto data, SerializationContext context)
        {
            var jsonResult = JsonSerializer.Serialize(data, _jsonSerializerOptions);
            var result = Serializers.Utf8.Serialize(jsonResult, context);
            return result;
        }
    }
}
