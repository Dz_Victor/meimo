using System.Collections.Generic;

namespace Meimo.Friends.Core
{
    public class PagedResponse<T>
    {
        public IReadOnlyList<T> Data { get; set; }
        public bool Paged { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
        public int TotalRecords { get; set; }

        public PagedResponse(
            IReadOnlyList<T> data,
            bool paged,
            int pageNumber,
            int pageSize,
            int totalPages,
            int totalRecords)
        {
            Data = data;
            Paged = paged;
            PageNumber = pageNumber;
            PageSize = pageSize;
            TotalPages = totalPages;
            TotalRecords = totalRecords;
        }

        public PagedResponse(IReadOnlyList<T> data)
        {
            Data = data;
        }
    }
}
