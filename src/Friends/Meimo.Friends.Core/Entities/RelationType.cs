namespace Meimo.Friends.Core
{
    public enum RelationType
    {
        Unknown = 0,
        Friend = 1,
        RequestFrom = 2,
        RequestTo = 3,
        SubscriptionFrom = 4,
        SubscriptionTo = 5,
        BlockedBy = 6,
        HasBlocked = 7
    }
}
