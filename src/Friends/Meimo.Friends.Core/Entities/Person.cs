using System;

namespace Meimo.Friends.Core
{
    public class Person
    {
        public Guid Id { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public string? Avatar { get; set; }

        public string? City { get; set; }

        public string? Country { get; set; }

        public bool Locked { get; set; }

        public bool Deleted { get; set; }
    }
}
