namespace Meimo.Friends.Core
{
    public class Filter
    {
        public string? FirstName { get; set; }
        
        public string? LastName { get; set; }
        
        public string? City { get; set; }
    }
}
