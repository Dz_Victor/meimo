using System;

namespace Meimo.Friends.GraphQL.Models
{
    public record RelationshipInput
    {
        public Guid FirstPersonId { get; set; }
        
        public Guid SecondPersonId { get; set; }
    }
}
