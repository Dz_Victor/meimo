using System;
using System.Threading.Tasks;
using Meimo.Friends.Core;
using Meimo.Friends.GraphQL.TypeExtensions;
using Meimo.Friends.Infrastructure;
using Meimo.Friends.Infrastructure.Kafka;
using Meimo.Friends.Kafka.Contracts;
using Meimo.Friends.ProfileGrpcClient;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Neo4j.Driver;

namespace Meimo.Friends.GraphQL
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<FriendsDbSettings>(
                _configuration.GetSection(nameof(FriendsDbSettings)));

            services.AddSingleton<IFriendsDbSettings>(sp =>
                sp.GetRequiredService<IOptions<FriendsDbSettings>>().Value);

            services.AddSingleton<Neo4j.Driver.ILogger>(sp =>
                new DriverLogger(sp.GetRequiredService<ILogger<DriverLogger>>()));
            
            services.AddSingleton<IDriver>(
                sp => GraphDatabase.Driver(
                    sp.GetRequiredService<IFriendsDbSettings>().ConnectionString, 
                    AuthTokens.Basic(sp.GetRequiredService<IFriendsDbSettings>().User, 
                                     sp.GetRequiredService<IFriendsDbSettings>().Password), 
                builder => builder.WithLogger(sp.GetRequiredService<Neo4j.Driver.ILogger>())));

            services.AddGrpcClient<Profiles.ProfilesClient>(o => 
            {
                o.Address = new Uri(_configuration.GetConnectionString("ProfileGrpcServer"));
            });

            services.AddScoped<IPersonRepository, PersonRepository>();
            services.AddScoped<IRelationshipRepository, RelationshipRepository>();

            services.AddGraphQLServer()
                .AddQueryType(d => d.Name(Constants.QueryTypeName))
                    .AddTypeExtension<PersonQueries>()
                    .AddTypeExtension<RelationshipQueries>()
                .AddMutationType(d => d.Name(Constants.MutationTypeName))
                    .AddTypeExtension<PersonMutations>()
                    .AddTypeExtension<RelationshipMutations>();

            services.AddSingleton<KafkaProducer>();
            services.AddSingleton<FriendsEventMessageSerializer>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            DeliveryResultHandler.Logger = loggerFactory.CreateLogger<KafkaProducer>();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGraphQL();
                
                endpoints.MapGet("/", context =>
                {
                    context.Response.Redirect("/graphql");
                    return Task.CompletedTask;
                });
            });
        }
    }
}
