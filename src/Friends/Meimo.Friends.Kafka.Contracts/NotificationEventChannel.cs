namespace Meimo.Friends.Kafka.Contracts
{
    public enum NotificationEventChannel
    {
        None = 0,
        BrowserNotificationFeed = 1,
        BrowserToast = 2,
        BrowserDesktop = 3,
        Email = 4
    }
}