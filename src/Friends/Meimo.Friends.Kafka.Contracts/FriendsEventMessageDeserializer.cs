using System.Text.Json;

namespace Meimo.Friends.Kafka.Contracts
{
    public class FriendsEventMessageDeserializer
    {
        private readonly JsonSerializerOptions _jsonSerializerOptions;

        public FriendsEventMessageDeserializer()
        {
            _jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                IgnoreNullValues = true,
                WriteIndented = true,
            };
        }

        public FriendsEventMessage? Deserialize(string data)
        {
            return JsonSerializer.Deserialize<FriendsEventMessage>(data, _jsonSerializerOptions);
        }
    }
}