using System;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using Meimo.Friends.Core;
using Meimo.Friends.ProfileReplication.Contract;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Meimo.Friends.ProfileReplication
{
    public class Worker : BackgroundService
    {
        private readonly ConsumerConfig _consumerConfig;
        private readonly string _topic;
        private readonly IPersonRepository _personRepository;
        private readonly ILogger<Worker> _logger;
        private readonly string _profileReplicationKey = Constants.ProfileReplicationKey;

        private IConsumer<string, ProfileMiddleInfo> _kafkaConsumer;
        
        public Worker(IConfiguration config, IPersonRepository personRepository, ILogger<Worker> logger)
        {
            _personRepository = personRepository;
            _logger = logger;

            _consumerConfig = new ConsumerConfig();
            config.GetSection("Kafka:ConsumerSettings").Bind(_consumerConfig);
            _topic = config.GetValue<string>("Kafka:ReplicationProfileTopic");
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _kafkaConsumer = new ConsumerBuilder<string, ProfileMiddleInfo>(_consumerConfig)
                .SetValueDeserializer(new ProfileMiddleInfoDeserializer())
                .Build();
            _kafkaConsumer.Subscribe(_topic);
            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation($"Meimo.Friends.ProfileReplication service started ...");
            
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var consumeResult = _kafkaConsumer.Consume(stoppingToken);
                    var resultKey = consumeResult.Message.Key;
                    var resultValue = consumeResult.Message.Value;

                    _logger.LogInformation($">>>{resultKey}: {resultValue?.Id}," +
                                           $" timestamp {consumeResult.Message.Timestamp.UtcDateTime}");

                    if (resultKey != _profileReplicationKey)
                    {
                        _logger.LogWarning("Не задан или получен неверный ключ репликации:" +
                                           $" {resultKey}");
                        continue;
                    }

                    if (resultValue is null)
                    {
                        _logger.LogWarning("Полученное сообщение не содержит value");
                        continue;
                    }

                    if (string.IsNullOrWhiteSpace(resultValue.FirstName))
                    {
                        _logger.LogWarning("Получен невалидный объект, поле FirstName должно быть заполнено");
                        continue;
                    }
                    
                    await _personRepository.SetPersonProfileInfo(resultValue.ToPerson());
                }
                catch (OperationCanceledException)
                {
                    break;
                }
                catch (ConsumeException e)
                {
                    _logger.LogWarning(e, $"Consume error: {e.Error.Reason}");

                    if (e.Error.IsFatal)
                    {
                        // https://github.com/edenhill/librdkafka/blob/master/INTRODUCTION.md#fatal-consumer-errors
                        break;
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(e, $"Unexpected error: {e}");
                    break;
                }
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _kafkaConsumer.Close();
            _kafkaConsumer.Dispose();
            _logger.LogInformation($"Meimo.Friends.ProfileReplication service stopped.");
            return base.StopAsync(cancellationToken);
        }
    }
}