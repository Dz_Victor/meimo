﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Grpc.Net.Client;

namespace Meimo.NotificationFeed.GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            AppContext.SetSwitch(
                "System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            // The port number(5002) must match the port of the gRPC server.
            using var channel = GrpcChannel.ForAddress("http://localhost:5002");
            var client = new Notifications.NotificationsClient(channel);

            var personId = Guid.NewGuid().ToString();

            for (int i = 1; i <= 14; i++)
            {
                var eventId = Guid.NewGuid().ToString();
                var reply = await client.CreateNotificationAsync(new CreateNotificationRequest
                {
                    PersonId = personId,
                    EventId = eventId,
                    Message = $"demo client message {i}"
                });
            }

            int pageCounter = 0;
            string parent = null;
            int pageSize = 3;
            string pageToken = null;
            int sortOrder = -1;

            Console.WriteLine($"personId = {personId}\n");
            Console.WriteLine("Id".PadRight(26) + "EventId".PadRight(38) + "Message");

            do
            {
                Console.WriteLine($"\nPage {++pageCounter} pageToken = {pageToken ?? "null"}\n");

                var page = await client.GetPaginatedNotificationsAsync(new PaginatedNotificationsRequest
                {
                    PersonId = personId,
                    Parent = parent,
                    PageSize = pageSize,
                    PageToken = pageToken,
                    SortOrder = sortOrder
                });

                pageToken = page.NextPageToken;

                page.Notifications.ToList()
                    .ForEach(x => {
                        Console.WriteLine($"{x.Id}  {x.EventId}  {x.Message}");
                    });
            } while (!string.IsNullOrWhiteSpace(pageToken));

            Console.WriteLine("\n Press any key to continue ...");
            Console.ReadKey();
        }
    }
}
