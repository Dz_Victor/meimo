﻿using System;
using System.Runtime.Serialization;

namespace Meimo.NotificationFeed.Core
{
    public class NotificationNotFoundException : Exception
    {
        public NotificationNotFoundException() { }

        public NotificationNotFoundException(string message) : base(message) { }

        public NotificationNotFoundException(string message, Exception inner) : base(message, inner) { }

        protected NotificationNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
